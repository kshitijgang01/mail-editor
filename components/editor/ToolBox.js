import {useEditor} from "@craftjs/core";
import Label from './selectors/Label'
import {ContainerCol2} from "./selectors/containers/Container-col-2";

export default () => {
    const {connectors, query} = useEditor();

    return (

        <div className="grid grid-cols-1 items-center justify-center">

            <button
                ref={ref => connectors.create(ref, <Label text={"lol"}/>)}
                className="inline-flex text-white bg-indigo-500 border-0 py-1 px-4 focus:outline-none hover:bg-indigo-600 rounded">
                Label
            </button>
            <br/>
            <button
                ref={ref => connectors.create(ref, <ContainerCol2/>)}
                className="inline-flex text-white bg-indigo-500 border-0 py-1 px-4 focus:outline-none hover:bg-indigo-600 rounded">
                Container-Col-2
            </button>


        </div>

    )
};