import {Editor, Frame, Element, Selector} from "@craftjs/core";
import Label from './selectors/Label'
import Hyperlink from "./selectors/Hyperlink";
import Container from './selectors/containers/Container'
import ToolBox from "./ToolBox";
import {ContainerCol2, RightContainer, LeftContainer} from './selectors/containers/Container-col-2'
import {Fragment} from 'react'

function FormEditor() {
    return (<div className="w-full justify-center container">
        <header>Drag and Drop form builder</header>
        <br/>
        <br/>
        <br/>

        <Editor resolver={{Label, Container, ContainerCol2, LeftContainer, RightContainer, Fragment, Hyperlink}}
                className="w-screen">
            <div className="flex flex-row justify-center items-center gap-4 h-auto">
                <div className="bg-amber-200 min-h-[400px]" style={{width: "600px"}}>
                    <Frame>
                        <Element canvas is={Container} width="640px" height="auto"
                                 background={{r: 255, g: 255, b: 255, a: 1}}

                                 custom={{displayName: 'App'}}>
                            <Label text={"lol"}/>
                        </Element>

                    </Frame>
                </div>

                <ToolBox/>

            </div>

        </Editor>


    </div>)
}

export default FormEditor