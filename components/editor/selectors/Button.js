import {useNode} from "@craftjs/core";

function Button() {
    const {connectors: {connect}} = useNode();
    return (<button
        ref={connect}
        className="inline-flex text-white bg-indigo-500 border-0 py-1 px-4 focus:outline-none hover:bg-indigo-600 rounded">
        Button
    </button>)
}

export default Button