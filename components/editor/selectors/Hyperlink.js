import {useNode} from "@craftjs/core";

function Hyperlink({children,url}){
    const {connectors: {connect}} = useNode();
    return(
        <a className="width-full justify-center items-center" ref={connect} href={url}>{children}</a>
    )
}

export default Hyperlink