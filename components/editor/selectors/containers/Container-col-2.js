import {useNode, Element} from "@craftjs/core";
import {Fragment} from 'react'

export const LeftContainer = ({children}) => {
    const {connectors: {connect}} = useNode();
    return (
        <div ref={connect} className="container items-center justify center w-full p-5">
            {children}
        </div>
    )
}

export const RightContainer = ({children}) => {
    const {connectors: {connect}} = useNode();
    return (
        <div ref={connect} className="container items-center justify center w-full p-5">
            {children}
        </div>
    )
}

export const ContainerCol2 = () => {
    return (
        <div className="grid grid-cols-2 gap-2 bg-blue-500 p-5 w-full">
            <Element id={"left-container"} is={LeftContainer} canvas>
                <Fragment></Fragment>
            </Element>
            <Element id={"right-container"} is={RightContainer} canvas>
                <Fragment></Fragment>
            </Element>
        </div>
    )
}

