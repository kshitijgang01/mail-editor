import dynamic from 'next/dynamic'
import {Fragment} from 'react'

import Editor from '../../components/editor/main'


export default function Home() {
    return (
        <Fragment>
            <Editor/>
        </Fragment>
    )
}